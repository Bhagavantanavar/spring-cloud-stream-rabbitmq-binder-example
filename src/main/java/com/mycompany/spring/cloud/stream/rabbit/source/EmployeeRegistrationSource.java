package com.mycompany.spring.cloud.stream.rabbit.source;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EmployeeRegistrationSource {

	@Output("employeeRegistrationSource")
	MessageChannel employeeRegistration();

}
